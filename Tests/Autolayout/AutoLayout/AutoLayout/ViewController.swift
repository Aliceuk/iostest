//
//  ViewController.swift
//  AutoLayout
//
//  Created by alice on 2016/2/18.
//  Copyright © 2016年 alice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let topView = UITextField()
    let bottomView = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.backgroundColor = UIColor.blueColor()
        topView.text = "ARE"
        
        
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = UIColor.redColor()
        topView.text = "Here"
        
        let finalButton   = UIButton()
        finalButton.backgroundColor = UIColor.darkGrayColor()
        finalButton.setTitle("Click me!", forState: UIControlState.Normal)
//        finalButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        finalButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(topView)
        self.view.addSubview(bottomView)
        self.view.addSubview(finalButton)
        
        var viewDict = Dictionary<String, UIView>()
        viewDict["top"] = topView
        viewDict["bottom"] = bottomView
        viewDict["final"] = finalButton
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[top]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[bottom]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[final]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewDict))

        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20.0-[top(bottom)]-[bottom(final)]-[final]-150.0-|", options: NSLayoutFormatOptions(rawValue:0), metrics: nil, views: viewDict))

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        topView.endEditing(true)
        bottomView.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }

}

