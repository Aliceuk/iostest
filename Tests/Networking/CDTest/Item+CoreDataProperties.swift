//
//  Item+CoreDataProperties.swift
//  
//
//  Created by alice on 2016/2/19.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Item {

    @NSManaged var done: NSNumber?
    @NSManaged var image: String?
    @NSManaged var task: String?

}
