//
//  ViewController.swift
//  CDTest
//
//  Created by alice on 2016/2/17.
//  Copyright © 2016年 alice. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON
let commonDeviceWeight = UIScreen.mainScreen().bounds.width
let commonDeviceHeight = UIScreen.mainScreen().bounds.height

class ViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource{

    var json: JSON = JSON.null
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let fetchRequest = NSFetchRequest(entityName:"Item")
    let segItems = ["ToDo", "Done", "All"]
    var todoGoods : [Item] = []
    var doneGoods : [Item] = []
    var allGoods  : [Item] = []
    var presentGoods : [Item] = []
    var tableView = UITableView()
    
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest(entityName: "Item")
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "done", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
        
        // Fetch Record
        allGoods = fetchedResultsController.fetchedObjects as! Array
        todoGoods = allGoods.filter({ $0.done == 0})
        doneGoods = allGoods.filter({ $0.done == 1})
        presentGoods = todoGoods
        print("loadAlready",allGoods)
        
        self.setUpSegmentedControlView()
        self.setUpTableView()
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpSegmentedControlView(){
        // Initialize
        let customSC = UISegmentedControl(items: segItems)
        customSC.selectedSegmentIndex = 0
        customSC.backgroundColor = UIColor.clearColor()
        customSC.frame = CGRectMake(0 , 40, commonDeviceWeight, 40)
        customSC.addTarget(self, action: "changeColor:", forControlEvents: .ValueChanged)
        self.view.addSubview(customSC)
    }
    
    func setUpTableView(){
        tableView.frame = CGRectMake(0, 80, commonDeviceWeight, commonDeviceHeight - 80)
        tableView.registerClass(GoodsCell.self, forCellReuseIdentifier: "cell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.estimatedRowHeight = 44
        tableView.delegate = self;
        tableView.dataSource = self;
        self.view.addSubview(tableView)
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        
        return 0
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentGoods.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! GoodsCell
        configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    
    
    func changeColor(sender: UISegmentedControl) {
        //"ToDo", "Done", "All"
        
        switch sender.selectedSegmentIndex {
        case 0:
            presentGoods = todoGoods
            break
        case 1:
            presentGoods = doneGoods
            break
        case 2:
            presentGoods = allGoods
            break
        default:
            break
        }
        tableView.reloadData()
    }
    
    func configureCell(cell: GoodsCell, atIndexPath indexPath: NSIndexPath) {
        
        let record = presentGoods[indexPath.row]

        // Update Cell
        if let name = record.task {
            cell.itemLabel.text = name
        }
        
        if let url = NSURL(string: record.image!){
            let data = NSData(contentsOfURL: url)
            cell.itemImage.image = UIImage(data: data!)
        }

        if let doneBool = record.done {
            if ( doneBool == 0) {
                cell.doneImage.image = UIImage(named:"todo")
            }else {
                cell.doneImage.image = UIImage(named:"done")
            }
        }
    }

        
}

