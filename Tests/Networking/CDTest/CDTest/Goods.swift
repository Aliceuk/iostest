//
//  Goods.swift
//  CDTest
//
//  Created by alice on 2016/2/18.
//  Copyright © 2016年 alice. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class Goods: NSManagedObject {
    @NSManaged var task: String?
    @NSManaged var image: String?
    @NSManaged var done: NSNumber
}
