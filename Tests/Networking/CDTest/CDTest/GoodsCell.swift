//
//  GoodsCell.swift
//  CDTest
//
//  Created by alice on 2016/2/19.
//  Copyright © 2016年 alice. All rights reserved.
//

import UIKit

class GoodsCell: UITableViewCell {

    var itemImage   = UIImageView();
    var itemLabel   = UILabel();
    var doneImage   = UIImageView();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        itemImage.layer.cornerRadius = itemImage.frame.size.width / 2;
        itemImage.clipsToBounds = true;
        
        itemImage.translatesAutoresizingMaskIntoConstraints = false
        itemLabel.translatesAutoresizingMaskIntoConstraints = false
        doneImage.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(itemImage)
        contentView.addSubview(itemLabel)
        contentView.addSubview(doneImage)
        
        
        //Set layout
        var viewsDict = Dictionary <String, UIView>()
        viewsDict["itemImage"] = itemImage;
        viewsDict["itemLabel"] = itemLabel;
        viewsDict["doneImage"] = doneImage;
        //Image
        

        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[itemImage(100)]-[itemLabel]-[doneImage(50)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict));
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[itemImage(100)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict));
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[itemLabel]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict));
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-25-[doneImage(50)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict));
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
